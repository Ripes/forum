package de.ripes.mcs;

import org.bukkit.plugin.java.JavaPlugin;

public class MCS extends JavaPlugin{
	
	public void onEnable() {
		System.out.println("[MCS] Enabled");
		MySQLFile file = new MySQLFile();
		file.setStandard();
		file.readData();
		MySQL.connect();
	}
	
	public void onDisable() {
		System.out.println("[MCS] Disabled");
		MySQL.disConnect();
	}
}
